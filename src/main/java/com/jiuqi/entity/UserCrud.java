package com.jiuqi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author JiuQi 玖柒
 * @Date 2021/10/31
 * @Emm...
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCrud {
    private int id;
    private String name;
    private int age;
    private String sex;
    private String classes;
    private String address;
}
