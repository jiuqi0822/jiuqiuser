package com.jiuqi.service;

import com.jiuqi.entity.UserCrud;

import java.util.List;

/**
 * @Author JiuQi 玖柒
 * @Date 2021/10/31
 * @Emm...
 */
public interface UserCrudService {
    List<UserCrud> showAllUsers();

    int addUser(UserCrud userCrud);

    int delUser(int id);

    UserCrud selUserById(int id);

    int updUser(UserCrud userCrud);
}
