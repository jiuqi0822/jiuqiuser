package com.jiuqi.service.impl;

import com.jiuqi.dao.UserCrudDao;
import com.jiuqi.entity.UserCrud;
import com.jiuqi.service.UserCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author JiuQi 玖柒
 * @Date 2021/10/31
 * @Emm...
 */
@Service
public class UserCrudServiceImpl implements UserCrudService {

    @Autowired
    private UserCrudDao userDao;

    @Override
    public List<UserCrud> showAllUsers() {
        return userDao.showAllUsers();
    }

    @Override
    public int addUser(UserCrud userCrud) {
        return userDao.addUser(userCrud);
    }

    @Override
    public int delUser(int id) {
        return userDao.delUser(id);
    }

    @Override
    public UserCrud selUserById(int id) {
        return userDao.selUserById(id);
    }

    @Override
    public int updUser(UserCrud userCrud) {
        return userDao.updUser(userCrud);
    }
}
