package com.jiuqi.dao;

import com.jiuqi.entity.UserCrud;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author JiuQi 玖柒
 * @Date 2021/10/31
 * @Emm...
 */
@Mapper
@Repository
public interface UserCrudDao {

    @Select("select * from user_crud")
    List<UserCrud> showAllUsers();

    @Insert("insert into user_crud (name,age,sex,classes,address) values (#{name},#{age},#{sex},#{classes},#{address})")
    int addUser(UserCrud userCrud);

    @Delete("delete from user_crud where id = #{id}")
    int delUser(int id);

    @Select("select * from user_crud where id = #{id}")
    UserCrud selUserById(int id);

    @Update("update user_crud set name=#{name},age=#{age},sex=#{sex},classes=#{classes},address=#{address} where id=#{id}")
    int updUser(UserCrud userCrud);
}
