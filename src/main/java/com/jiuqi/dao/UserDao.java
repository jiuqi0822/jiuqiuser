package com.jiuqi.dao;

import com.jiuqi.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

/**
 * @Author JiuQi 玖柒
 * @Date 2021/11/08
 * @Emm...
 */
@Mapper
@Repository
public interface UserDao {

    /**
     * 注册，插入数据
     * @param user
     */
    @Insert("insert into user_lr (username,password,email) values (#{username},#{password},#{email})")
    void insertUser(User user);

    /**
     * 根据邮箱查询
     * @param email
     * @return
     */
    @Select("select * from user_lr where email = #{email}")
    User queryByEmail(String email);

    /**
     * 登录
     * @param user
     * @return
     */

    @Select("select * from user_lr where username = #{username} and password = #{password}")
    User selectUser(User user);

    /**
     * 检验username和email是否存在
     * @param user
     * @return
     */
    @Select("select * from user_lr where username= #{username} and email = #{email}")
    User selectUserByUsername(User user);

    @Update("update user_lr set password = #{password} where username = #{username} and email = #{email}")
    void updateUser(User user);
}
