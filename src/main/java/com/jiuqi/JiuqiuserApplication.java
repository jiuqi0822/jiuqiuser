package com.jiuqi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JiuqiuserApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiuqiuserApplication.class, args);
    }

}
