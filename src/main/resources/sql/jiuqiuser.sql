/*
 Navicat Premium Data Transfer

 Source Server         : 本地MySQL
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : jiuqiuser

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 08/11/2021 19:46:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_crud
-- ----------------------------
DROP TABLE IF EXISTS `user_crud`;
CREATE TABLE `user_crud`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` int(0) NULL DEFAULT NULL,
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `classes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_crud
-- ----------------------------
INSERT INTO `user_crud` VALUES (2, '小钱', 23, '男', '移动5班', '河南省南阳');
INSERT INTO `user_crud` VALUES (12, '小钱', 65, '男', '移动5班', '河南省南阳');
INSERT INTO `user_crud` VALUES (29, '小胡', 51, '女', '移动3班', '河南省南阳市');
INSERT INTO `user_crud` VALUES (32, '阿斯蒂芬', 23, '女', '移动3班', '河南省');
INSERT INTO `user_crud` VALUES (36, '老李', 23, '男', '大数据', '河南省');
INSERT INTO `user_crud` VALUES (42, '阿斯蒂', 12, '男', '1212', '213123');

-- ----------------------------
-- Table structure for user_lr
-- ----------------------------
DROP TABLE IF EXISTS `user_lr`;
CREATE TABLE `user_lr`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_lr
-- ----------------------------
INSERT INTO `user_lr` VALUES (6, '张三', '123456', '1970630117@qq.com');
INSERT INTO `user_lr` VALUES (7, '李四', '123456', '1970630118@qq.com');
INSERT INTO `user_lr` VALUES (22, 'manager', 'asdfasdfasdf', '1970630118@qq.com');
INSERT INTO `user_lr` VALUES (23, '王五', 'newpassword', '1223334444@qq.com');
INSERT INTO `user_lr` VALUES (24, '123456', '123456', '1970630118@qq.com');
INSERT INTO `user_lr` VALUES (25, '122333', '122333', '1970630118@qq.com');

SET FOREIGN_KEY_CHECKS = 1;
